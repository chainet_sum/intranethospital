import Axios from "axios";

const state = {
  testing: "123456789",
  news: [
    {
      Id: 1,
      Date: "0000-00-00 00:00:00",
      Nname: "test1",
      Path: "C://xxx/test111"
    },
    {
      Id: 2,
      Date: "2019-08-13 14:14:30",
      Nname: "test1",
      Path: "C://test11/xxx"
    }
  ],
  news2: []
};

const actions = {
  loadpost({ commit }) {
    Axios.get("http://localhost/slimdb/public/api/getnews").then(response => {
      console.log(response.data);
      let news2 = response.data;
      commit("set_news2", news2);
    });
  }

  // loadData() {
  //   this.axios
  //     .get("http://localhost/slimdb/public/api/getnews")
  //     .then(response => response.data)
  //     .then(news2 => {
  //       console.log(news2);
  //     });
  //   }
};
const mutations = {
  set_news2(state, news2) {
    state.news2 = news2;
  }
};
const getters = {
  news: state => {
    return state.news;
  },
  news2: state => {
    return state.news2;
  }
};

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
};
